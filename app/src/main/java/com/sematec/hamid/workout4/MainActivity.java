package com.sematec.hamid.workout4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sematec.hamid.workout4.Adapder.ListTempsAdapter;
import com.sematec.hamid.workout4.Model.ListTempModel;
import com.sematec.hamid.workout4.Model.ListTempsDBModel;
import com.sematec.hamid.workout4.Model.TempModel;
import com.sematec.hamid.workout4.Model.YahooModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity {

    ImageView background;
    TextView cityNameView, tempCity, chargeView;
    EditText newCity;
    Button citybtn;
    ListView listtemp;
    BroadcastReceiver connectreceiver, disconnectreceiver;
    String city, cabl;
    String urlCity, urlCityTemp;
    private int Hour;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        background = (ImageView) findViewById(R.id.background);


        cityNameView = (TextView) findViewById(R.id.cityNameView);
        tempCity = (TextView) findViewById(R.id.tempCity);
        chargeView = (TextView) findViewById(R.id.chargeView);
        newCity = (EditText) findViewById(R.id.newCity);
        citybtn = (Button) findViewById(R.id.citybtn);
        listtemp = (ListView) findViewById(R.id.listtemp);

        findViewById(R.id.chargeView).setVisibility(View.GONE);
        Calendar d = Calendar.getInstance();
        int Hour = d.getTime().getHours();


        getbackgrand(Hour);
        getconnect(cabl);
        getCity(city);
        citybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                city = newCity.getText().toString();
                getWeatherByAsync(city);
                newCity.setText("");
            }
        });


    }

    public void getCity(final String city) {

        String urlCity = getString(R.string.ipcityname);

        final AsyncHttpClient cityClient = new AsyncHttpClient();
        cityClient.get(urlCity, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (city == null) {
                    List<TempModel> m = TempModel.listAll(TempModel.class);
                    TempModel tmm = m.get(m.size() - 1);
                    cityNameView.setText(tmm.getCity());
                    tempCity.setText(tmm.getTemp());

                    List<ListTempModel> temps = new ArrayList<>();
                    List<ListTempsDBModel> fulldb = ListTempsDBModel.listAll(ListTempsDBModel.class);

                    for (int l = 0; l < 9; l++) {
                        ListTempModel temp = new ListTempModel();
                        temp.setName(fulldb.get(l).getName());
                        temp.setHigh(fulldb.get(l).getHigh());
                        temp.setLow(fulldb.get(l).getLow());
                        temps.add(temp);
                    }
                    ListTempsAdapter adapter = new ListTempsAdapter(MainActivity.this,temps);
                    listtemp.setAdapter(adapter);


                }


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                getCityFromJson(responseString);
            }
        });

    }

    public void getCityFromJson(String responseString) {
        try {
            JSONObject cityobj = new JSONObject(responseString);

            city = cityobj.getString("city");


            getWeatherByAsync(city);

        } catch (Exception e) {
            ShowToast("Error" + e);
        }
    }

    public void getWeatherByAsync(String city) {

        final String urlCityTemp = getString(R.string.yahoo_weather_part1)
                + city + getString(R.string.yahoo_weather_part2);

        AsyncHttpClient tempClient = new AsyncHttpClient();
        tempClient.get(urlCityTemp, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                ShowToast("Error" + throwable);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                getTempByGson(responseString);

            }
        });
    }

    public void getTempByGson(String responseString) {
        Gson gson = new Gson();
        YahooModel tempModel = gson.fromJson(responseString, YahooModel.class);

        String nameofcity = tempModel.getQuery().getResults().getChannel().getLocation().getCity();
        setResultCity(nameofcity);

        String temp = tempModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        setResult(temp);

        double c = (Integer.parseInt(temp) - 32) / 1.8;
        final int cint = (int) c;
        String tempint = valueOf(cint);

        List<TempModel> tpmdel = TempModel.listAll(TempModel.class);
        TempModel.deleteAll(TempModel.class);

        TempModel tpm = new TempModel(nameofcity, tempint);
        tpm.save();

        getListTempByGson(responseString);
    }

    private void getListTempByGson(String responseString) {
        Gson listGson = new Gson();
        YahooModel listTempModel = listGson.fromJson(responseString, YahooModel.class);

        List<ListTempModel> temps = new ArrayList<>();

        List<ListTempsDBModel> itdbdel = ListTempsDBModel.listAll(ListTempsDBModel.class);
        ListTempsDBModel.deleteAll(ListTempsDBModel.class);


        for (int i = 1; i < 10; i++) {

            ListTempModel temp = new ListTempModel();

            String nameTemp = listTempModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getDay();
            temp.setName(nameTemp);

            String highTemp = listTempModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getHigh();
            double ht = (Integer.parseInt(highTemp) - 32) / 1.8;
            final int htint = (int) ht;
            String highint = valueOf(htint);
            temp.setHigh(highint);

            String lowTemp = listTempModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getLow();
            double lt = (Integer.parseInt(lowTemp) - 32) / 1.8;
            final int ltint = (int) lt;
            String lowint = valueOf(ltint);
            temp.setLow(lowint);

            temps.add(temp);

            ListTempsDBModel ltdb = new ListTempsDBModel(nameTemp, highint, lowint);
            ltdb.save();
        }

        ListTempsAdapter adapter = new ListTempsAdapter(this, temps);
        listtemp.setAdapter(adapter);


    }

    public void setResultCity(final String nameofcity) {
        cityNameView.setText(nameofcity);

    }

    public void setResult(String temp) {
        double c = (Integer.parseInt(temp) - 32) / 1.8;
        final int cint = (int) c;
        tempCity.setText(valueOf(cint));


    }

    public void ShowToast(final String str) {
        android.widget.Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();

    }

    public void getconnect(String cabl) {

        connectreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                findViewById(R.id.newcitylayout).setVisibility(View.GONE);
                findViewById(R.id.chargeView).setVisibility(View.VISIBLE);


                IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                Intent batteryStatus = context.registerReceiver(null, ifilter);

                int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

                float batteryPct = (level / (float) scale) * 100;

                chargeView.setText((int) batteryPct + "% Charge");


                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //Screen on

            }
        };

        IntentFilter chargeFilter = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");

        registerReceiver(connectreceiver, chargeFilter);

        disconnectreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                findViewById(R.id.chargeView).setVisibility(View.GONE);
                findViewById(R.id.newcitylayout).setVisibility(View.VISIBLE);
            }
        };
        IntentFilter disconnectFilter = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");

        registerReceiver(disconnectreceiver, disconnectFilter);


    }

    public void getbackgrand(int Hour) {

        if (Hour >= 5 & Hour < 11)
            background.setBackgroundResource(R.drawable.wallpaper_1);

        else if (Hour >= 11 & Hour < 16)
            background.setBackgroundResource(R.drawable.wallpaper_2);

        else if (Hour >= 16 & Hour < 21)
            background.setBackgroundResource(R.drawable.wallpaper_3);

        else if (Hour >= 21 & Hour < 24)
            background.setBackgroundResource(R.drawable.wallpaper_4);
        else
            background.setBackgroundResource(R.drawable.wallpaper_4);

    }

    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(connectreceiver);
        unregisterReceiver(disconnectreceiver);
    }
}
