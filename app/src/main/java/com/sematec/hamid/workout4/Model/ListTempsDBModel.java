package com.sematec.hamid.workout4.Model;

import com.orm.SugarRecord;

/**
 * Created by Hamid on 30/08/2017.
 */

public class ListTempsDBModel extends SugarRecord<ListTempsDBModel> {

    String name;
    String high;
    String low;

    public ListTempsDBModel() {
    }

    public ListTempsDBModel(String name, String high, String low) {
        this.name = name;
        this.high = high;
        this.low = low;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }
}
