package com.sematec.hamid.workout4.Adapder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sematec.hamid.workout4.MainActivity;
import com.sematec.hamid.workout4.Model.ListTempModel;
import com.sematec.hamid.workout4.Model.ListTempsDBModel;
import com.sematec.hamid.workout4.R;

import java.util.List;

/**
 * Created by Hamid on 30/08/2017.
 */

public class ListTempsAdapter extends BaseAdapter {

    Context mContext;
    List<ListTempModel> temps;

    public ListTempsAdapter(Context mContext, List<ListTempModel> temps) {
        this.mContext = mContext;
        this.temps = temps;
    }


    @Override
    public int getCount() {
        return temps.size();
    }

    @Override
    public Object getItem(int position) {
        return temps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.temp_list_item, viewGroup, false);

        TextView itemName = (TextView) rowView.findViewById(R.id.itemName);
        TextView itemHighTemp = (TextView) rowView.findViewById(R.id.itemHighTemp);
        TextView itemLowTemp = (TextView) rowView.findViewById(R.id.itemLowTemp);

        itemName.setText(temps.get(position).getName());
        itemHighTemp.setText(temps.get(position).getHigh());
        itemLowTemp.setText(temps.get(position).getLow());


        return rowView;
    }
}
