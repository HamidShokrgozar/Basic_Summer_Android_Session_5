package com.sematec.hamid.workout4.Model;

import com.orm.SugarRecord;

/**
 * Created by Hamid on 29/08/2017.
 */

public class TempModel extends SugarRecord<TempModel> {

    String city;
    String temp;

    public TempModel() {
    }

    public TempModel(String city, String temp) {
        this.city = city;
        this.temp = temp;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
