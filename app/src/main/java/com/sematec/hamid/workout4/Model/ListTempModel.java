package com.sematec.hamid.workout4.Model;

/**
 * Created by Hamid on 30/08/2017.
 */

public class ListTempModel {

    String name;
    String high;
    String low;

    public ListTempModel() {
    }

    public ListTempModel(String name, String high, String low) {
        this.name = name;
        this.high = high;
        this.low = low;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }
}
